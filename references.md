## Pour aller plus loin

- Pourquoi choisir TYPO3 : <br/>https://typo3.fr/a-propos/pourquoi-typo3#c6283
- Documentation : https://docs.typo3.org/
- Demandez à la communauté : https://typo3.org/help
- Programme "Send your Junior" : <br/>https://typo3.com/services/send-your-junior


## Pour aller plus loin

- Extension pour lancer un site demo <br/>(exemple de templating et contenu) : <br/>[typo3/cms-introduction](https://packagist.org/packages/typo3/cms-introduction)
- Créer une extension extbase : <br/>[Extension Builder](https://docs.typo3.org/p/friendsoftypo3/extension-builder/9.10/en-us/)