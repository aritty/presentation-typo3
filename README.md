# Présentation TYPO3

## Utilisation

Le slider est généré automatiquement au push sur la branche `master` et visible ici  https://aritty.gitlab.io/presentation-typo3/

## Ressources

Réaliser avec https://revealjs.com

Baser sur le projet https://gitlab.com/gitlab-org/end-user-training-slides pour les pipelines de Gitlab CI

Exemples de Gitlab Pages Générées https://gitlab-org.gitlab.io/end-user-training-slides/