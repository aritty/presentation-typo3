## TYPO3 Généralités <!-- .element: class="title" -->

- CMS PHP depuis 1997 et Open source depuis 2000 
- Une philosophie orientée vers une utilisation pro
- Une Association et une Société : https://typo3.fr/societe/ce-que-nous-faisons
  - Promouvoir TYPO3 et la Communauté  
  - Orienter et "Professionnaliser" les évolutions
  - Former la communauté
- Communauté + Extensions 

Note: 

- Anecdote Dassaut
- Anecdote Gestion dépendance + fichiers modifié sur le serveur
- Association : et surtout merci pour la doc !
- Former : 
  - send-your-junior Gratuit jusque 12 mois (juste le salaire et l'hebergement à Dusseldorf)
  - Code sprint
  - ... 
- En évolution constante https://typo3.org/community/teams/typo3-development/initiatives/


----------
TYPO3 pour les Utilisateurs <!-- .element: class="title" -->

- Backend séparé du Frontend : <br/>Interface, Users et Cache
- Configuration granulaire des droits : 
  <br/>Module/outils Accessibles, Types de Contenus/éléments, Champs d'édition  
- Axé sur l'arborescence : <br/>Configuration, Accès, "Clarté" <!-- .element: class="fragment" data-fragment-index="1" -->
      
Note: 

- Backend => pas d'overlay sur le site en front mais moins "site centré" si on veut étendre pour gérer d'autres apps que le site + sécurité 
- Pas d'apriori sur le rendu Frontend (utilisation de framework css, js de vote choix) ... parfois un peu déroutant => cf. ref "site demo"


<!-- .slide: data-background-image="images/typo3_users_1.jpg" data-background-position="left" data-background-size="contain" -->
<aside class="overlay overlay-right">

Arborescence : Contributeurs <!-- .element: class="title" -->

- Représentation claire du site
- Différents types de pages et de gabarits
- Visualisation de la page sur laquelle on travaille

</aside>

Note: 

- digression sur ce que la gestion par arbo apporte


<!-- .slide: data-background-image="images/typo3_users_2.jpg" data-background-position="left" data-background-size="contain" -->
<aside class="overlay overlay-right">

Arborescence et droits granulaires : Administration <!-- .element: class="title" -->

- Configuration, héritée par branche
- Droits et Accès, inspiré de linux
- Interface sur mesure pour les contributeurs

</aside>


----------
<!-- .slide: data-background-image="images/module_page.gif" data-background-position="center left 10px" data-background-size="60%"  -->
<aside class="overlay overlay-right">

Gestions des contenus <!-- .element: class="title" -->

- Gabarits de page + <br/>Différents types de contenu
- Glisser / Déposer
- Menu contextuel
</aside>

Note: 

À placer dans les gabarits de page
- Titre / Texte 
- Images / Media
- liste de Fichiers / Pages (menus)
- Formulaires / Login


<!-- .slide: data-background-image="images/images.gif" data-background-position="center left 10px" data-background-size="auto 50%" data-auto-animate -->
<aside class="overlay overlay-right">

Gestions des contenus <!-- .element: class="title" -->

- Champs de Configuration <br/>+ CKEditor
- Fichiers et Images 
  <br>Arbo avec droits
- Crop et Resize d'Images

</aside>
Note: 
Gestion des espaces de stockage avec droits et montage par user 

Compatible avec des types d'image non "web" si vous avez installé imagemagik sur le serveur 
Resize en fonction de la zone et des conf

Choix du crop (suivant taille d'écran si configuré) et conf du resize (par rapport à la zone)

Images à partir d'un pdf (thumbnail) - sous reserve de Postscript et imagemagik sur le serveur


----------
<!-- .slide: data-background-image="images/module_list_1.jpg" data-background-position="left" data-background-size="50%" -->
<aside class="overlay overlay-right">

Listes d'Items <!-- .element: class="title" -->
- Colonnes affichées 
- Copier / Couper / Coller + <br/>Mode Multiple
- Recherche
- Edition Multiple
- ...

</aside>

Note: 

- Recherche sur page + dans l'arbo


----------
Nativement :  <!-- .element: style="text-align: left;"  -->

- Multilingue 
- Multi site
- Formulaires
- Contenu Versionné 
- Workflow de publication
- Module "Vue" 
  <br/>Divers device / Workflow
  


<!-- .slide: data-background-image="images/langues.gif" data-background-position="left" data-background-size="50% 50%"  data-auto-animate -->
<aside class="overlay overlay-right">

Multilingue  <!-- .element: class="title" -->
- Visualisation des éléments traduits cote à cote
- Créer les contenus traduits
<br/> <small>(avec le "lien" avec le contenu de la langue par défaut)</small>  
- Visualisation des champs dans la langue par défaut 

</aside>

Note:
Visualisation des champs modifiés dans la langue par défaut 


<!-- .slide: data-background-image="images/multisite.jpg" data-background-position="left" data-background-size="contain" data-auto-animate -->
<aside class="overlay overlay-right">

Multi site <!-- .element: class="title" -->
 
- Nom de domaines 
- Langues
- ...

Arborescence et Droits <br/>=> gestion simplifiée
</aside>


<!-- .slide: data-background-image="images/forms.jpg" data-background-position="left" data-background-size="50% 50%" data-auto-animate -->
<aside class="overlay overlay-right">

Formulaires <!-- .element: class="title" -->
- Création visuelle 
  <br/><small>Champs, Étapes/Pages, Principaux Finisher...</small>
- Stockage fichier yaml
  <br/> <small>Versionnable, Configuration de certain finishers (stockage BDD)</small>
  
</aside>


<!-- .slide: data-background-image="images/version.jpg" data-background-position="left" data-background-size="50% 50%" data-auto-animate -->
<aside class="overlay overlay-right">

Contenu Versionné <!-- .element: class="title" -->
- Historique infini (sauf configuration contraire)
- Utilisateurs
</aside>


<!-- .slide: data-background-image="images/workflow.gif" data-background-position="left" data-background-size="50% 50%" data-auto-animate -->
<aside class="overlay overlay-right">

- Workflow avancés
- Module "Vue" 
  <br/>Divers device / Workflow

</aside>


----------
Et aussi :  <!-- .element: style="text-align: left;"  -->

- Champs Seo / Réécriture d'url
- Redirections
- Recherche Indexée
- Planificateur de taches
- Validateur de Liens
- Infos d'administration : <br/><small>Notifications, Logs, Nombre de références à un élément ...</small>
- ...
