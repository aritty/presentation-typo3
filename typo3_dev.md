### TYPO3 Généralités 

Core Team + Security Team  garantissant :
- Code de qualité
- Roadmap Claire et Fiable
- Suivi des versions et standards PHP 
- Utilisation de packages reconnus (Symfony)

![Roadmap](images/roadmap.png)	 <!-- .element: style="max-width:80%;margin-bottom: 0"  -->

Note:

- Core team : Accessible notamment pendant les Event
- Roadmap : prévisibilité / rotation "rapide" => version de php
- standard : 
  - PSR (même si inertie / compatibilité entre version)  
  - middelware => integration d'app / integration de code à "bas niveau"
  - ...
- Eval extension : Fonctions / Configurations obsolètes + lien vers les docs


<!-- .slide: data-background-image="images/scan_ext.jpg" data-background-position="center left 10px" data-background-size="auto 50%" -->
<aside class="overlay overlay-right" style="right:0">
Mission : faciliter la vie des Devs

- [Documentation](https://docs.typo3.org/m/typo3/reference-coreapi/10.4/en-us/) 
  <br/>Claire, Versionnée et Standardisée
- Outils d'évalution système
  <br/> <small> MaJ des schemas de BDD, Test de l'environnement, du traitement d'image, ...</small>
- Outils d'évalution des extensions
  <br/> <small>Deprecated + Lien vers les ChangeLog, ...</small>
- ...
</aside>


----------
### Étendre TYPO3 

Mode Composer <!-- .element: class="title" -->  

```json [7-12|26-28|46-55|57-61|72-78]
{
    "name": "tribu/site-example",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "authors": [],
    "require": {
        "typo3/minimal": "^8 || ^9 || ^10",
        "typo3/cms-felogin": "^10.4",
        "typo3/cms-form": "^10.4",
        "typo3/cms-indexed-search": "10.4",
        "typo3/cms-info": "^10.4",
        "typo3/cms-linkvalidator": "^10.4",
        "typo3/cms-recycler": "^10.4",
        "typo3/cms-redirects": "^10.4",
        "typo3/cms-reports": "^10.4",
        "typo3/cms-rte-ckeditor": "^10.4",
        "typo3/cms-scheduler": "^10.4",
        "typo3/cms-seo": "^10.4",
        "typo3/cms-viewpage": "^10.4",
        "typo3/cms-workspaces": "^10.4",
        "typo3/cms-introduction": "^4.2",

        "georgringer/news": "^8.3",
        "helhum/typo3-console": "^6.3"
    },
    "require-dev": {
        "typo3/cms-adminpanel": "^10.4",
        "typo3/cms-lowlevel": "^10.4",
        "typo3/testing-framework": ">4.13",
        "friendsofphp/php-cs-fixer": "^2.3.1",
        "phpstan/phpstan": ">0.11",
        "phpstan/extension-installer": "^1.0",
        "saschaegerer/phpstan-typo3": ">0.11",
        "overtrue/phplint": "^1.1",

    },
    "conflict": {
        "typo3/cms": "<8.7.10",
        "typo3/cms-core": "<8.7.10"
    },
    "autoload": {
        "psr-4": {
            "Tribu\\TcSiteTemplate\\": "public/typo3conf/ext/tc_site_template/Classes"
        }
    },
    "config": {
        "vendor-dir":  "private/vendor",
        "bin-dir": "private/bin"
    },
    "extra": {
        "typo3/cms": {
            "web-dir": "./public",
            "app-dir": "./"
        }
    },
    "scripts"     : {
        "post-autoload-dump": [
            "@db:schema-update",
            "@cache:flush-files"
        ],
        "db:schema-update": "typo3cms database:updateschema \"*.add,*.change\"",
        "db:schema-add": "typo3cms database:updateschema \"*.add\"",
        "db:schema-cleanup": "typo3cms database:updateschema \"destructive\"",
        "db:backup": "typo3cms database:export -c Default -e \"cf_*\" -e \"cache_*\" -e \"[bf]e_sessions\"  -e \"index_*\" -e \"tx_extensionmanager_*\" -e sys_log > backup.sql",
        "cache:flush-all": "typo3cms cache:flush",
        "cache:flush-files": "typo3cms cache:flush --files-only",
        "cache:flush-pages": "typo3cms cache:flushgroups pages,all",
        "cache:flush-system": "typo3cms cache:flushgroups system,all",
        "test:php:lint": [
            "phplint"
        ],
        "test:php:unit": [
            "phpunit -c Build/phpunit-unit.xml"
        ],
        "test": [
            "@test:php:lint",
            "@test:php:unit"
        ],
        "code:php-fixer": [
            "php-cs-fixer --diff -v fix"
        ]
    }
}
```

Note:
Digression sur Composer :
- les CMS sont trop rarement complétement intégré
- facilite déploiement et gestion multi environnement


### Étendre TYPO3 

[Créer une extension](https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ExtensionArchitecture/Index.html#) <!-- .element: target="_blank" -->

- Créer un Template
- Ajouter de nouveaux types de Contenu / Éléments 
- Ajouter des Fonctionnalités
- ...

Structure des dossiers toujours identiques 
<br/> => Facilité pour versionner et déployer le projet
 
Note:
- Middleware
- Service d'authentification ...


----------
### Étendre TYPO3 

Ajouter/ Étendre des Contenus et Items
- Tables : Déclaration SQL
- Gestion Backend et liens entre Items : 
  <br/> Tableau de configuration PHP [TCA](https://docs.typo3.org/m/typo3/reference-tca/master/en-us/) <!-- .element: target="_blank" -->
  <br/> <small>Champs d'édition, configuration multilingue, versions, recherche backend, ...</small>

\+ Framework interne (MVC, DDD, TDD) [Extbase](https://docs.typo3.org/m/typo3/book-extbasefluid/10.4/en-us/Index.html) <!-- .element: target="_blank" -->

Note:
- Partie la moins refondu depuis que je l'utilise / un peu vieillissant mais finalement bien efficace 
- SQL simplifié : Uniquement les champs custom ou à rajouter à une table  
- Configuration des fonctionnalités avancées de TYPO3 grace au TCA
- Partie plus moderne pour gérer les objets EXTBASE (front et ou Outils/module backend custom)
  - Collection de Classes de base (Controllers, Models, Repositories, Interfaces ...)
  - Typage fort
  - ...


<!-- .slide: data-background-image="images/editor-shows-typo3-fluid.png" data-background-position="center left 10px" data-background-size="40%" -->
### Étendre TYPO3 
<aside class="overlay-right" style="right:0">

Gérer les rendus Front / Templating
- Controller Front configuré via [TypoScript](https://docs.typo3.org/m/typo3/reference-typoscript/master/en-us/) <!-- .element: target="_blank" -->
  <br/> <small>Fichiers (Versionnable)
  <br/> et BDD (Override d'une conf sur une branche)</small>
- Moteur de template [Fluid](https://docs.typo3.org/other/typo3/view-helper-reference/9.5/en-us/Index.html) <!-- .element: target="_blank" -->
  <br/> <small>Language balisé (xml), <br/>lien avec le Typoscript / les Objets Extbase</small>
  
</aside>

Note:
- Typoscript :
  - Type de rendu (html, xml, json) et configuration
  - Récupérer des contenus et les rendre dispo pour le moteur de template
  - Injecter les assets (css, js) 
  - Et quelques fonctionnalités de rendu front : wrap de contenu avec des balises, remplacement de "markers", ....
- Fluid : 
 - balise xml + inline
 - XSD => integration facile au IDE


----------
### Et aussi :  <!-- .element: style="text-align: left;"  -->

- Compatible avec plusieurs BDD : <br/><small>Mysql/MariaDB, PostgreSQL, SQLlite</small> 
- Framework Cache poussé
  <br/> <small>avec gestion de différent type</small> 
- Étendre / Configurer la réécriture d'url <br/>Via fichiers yaml
- Étendre le moteur de template fluid : <br/>ViewHelpers
- ...

Note:
