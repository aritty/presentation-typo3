Alexandre RITTY 
![Alexandre RITTY](images/alex@2x.png)	 <!-- .element: style="max-width:10%;margin-bottom: 0"  -->				

Développeur depuis 2010 <br/> au sein de l'agence web <a href="https://tribu-and-co.fr/" target="_blank">Tribu and Co</a> à TOURS

Note:

- Agence web => type de projet / client
- débats cms vs framework cf. pres de l'afup tours


Un Cms, mais pourquoi ?
- "Rassurer" les clients => Communauté de Dev  <!-- .element: class="fragment" data-fragment-index="2" -->
- CMS : Content Management System
<br/>Séparation gestion et rendu des contenus
- Framework axés sur la gestion de contenu <!-- .element: class="fragment" data-fragment-index="1" -->

Note:
- projet / client : demande "centrale" sur la gestion de contenu
- Et même en s'appuyant sur un framework recréer le fonctionnement => décisions qui nous sont propres et difficultés de reprise du projet par un/des tierce


Bien garder en tête les contraintes
- Choisir (et Conserver) un CMS adapter au projet <!-- .element: class="fragment" data-fragment-index="2" -->
- S'approprier la "philosophie" du CMS
<br/><small>=> courbe d'apprentissage </small>
- L'"inertie" du CMS
<br/><small>=> prise en compte des nouvelles versions de php, des packages, sécurité, ...</small>

Note:
* Que l'on retrouve potentiellement aussi sur un projet complet et complexe reposant sur un framework
* Suivre la philosophie plutôt que d'aller contre
* Indispensable le choix d'un CMS adapter
<br/>Pres pour TYPO3 : inclue nativement énormément d'outils qui sauront se rendre indispensable pour la gestion d'un site mais aussi par exemple pour s'intégrer dans une galaxie de web app
							